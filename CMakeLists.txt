cmake_minimum_required(VERSION 3.7)
cmake_policy(VERSION 3.7)
project(vkpipeline-db)

find_package(Vulkan REQUIRED)

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fopenmp")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall")
if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wextra -Wundef")
    set(CMAKE_C_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wextra -Wundef")
endif(CMAKE_COMPILER_IS_GNUCXX)

file(GLOB SOURCES blob.c serialize.c)

add_library(VkLayer_vkpipeline_db SHARED vkpipeline_db.cpp ${SOURCES})

add_executable(run run.c ${SOURCES})
target_link_libraries(run ${Vulkan_LIBRARY})
